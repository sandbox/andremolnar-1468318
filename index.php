<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * The routines here dispatch control to the appropriate handler, which then
 * prints the appropriate page.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */


use Symfony\Component\ClassLoader\UniversalClassLoader;
use Symfony\Component\ClassLoader\ApcUniversalClassLoader;
use Drupal\Core\HttpFoundation\Request;
use Drupal\Core\Bootstrap\Bootstrap;

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/core/includes/config.inc';
/**
 * Initializes and returns the class loader.
 *
 * The class loader is responsible for lazy-loading all PSR-0 compatible
 * classes, interfaces, and traits (PHP 5.4 and later). Its only dependencies
 * are DRUPAL_ROOT and variable_get(). Otherwise it may be called as early as
 * possible.
 *
 * @return Symfony\Component\ClassLoader\UniversalClassLoader
 *   A UniversalClassLoader class instance (or extension thereof).
 */
// Include the Symfony ClassLoader for loading PSR-0-compatible classes.
require_once DRUPAL_ROOT . '/core/vendor/Symfony/Component/ClassLoader/UniversalClassLoader.php';
$loader = new UniversalClassLoader();
$loader->register();
     //if (function_exists('apc_store')) {
     //     require_once DRUPAL_ROOT . '/core/vendor/Symfony/Component/ClassLoader/ApcUniversalClassLoader.php';
     //     $loader = new ApcUniversalClassLoader('drupal.' . $GLOBALS['drupal_hash_salt']);
     //     break;
     //   }
// Register explicit vendor namespaces.
$loader->registerNamespaces(array(
  // All Symfony-borrowed code lives in /core/vendor/Symfony.
  'Symfony' => DRUPAL_ROOT . '/core/vendor',
));
// Register the Drupal namespace for classes in core as a fallback.
// This allows to register additional namespaces within the Drupal namespace
// (e.g., for modules) and avoids an additional file_exists() on the Drupal
// core namespace, since the class loader can already determine the best
// namespace match based on a string comparison. It further allows modules to
// register/overload namespaces in Drupal core.
$loader->registerNamespaceFallbacks(array(
  // All Drupal-namespaced code in core lives in /core/lib/Drupal.
  'Drupal' => DRUPAL_ROOT . '/core/lib',
));

$request = DrupalRequest::createFromGlobals();




require_once DRUPAL_ROOT . '/core/includes/bootstrap.inc';

global $bootstrap;

$bootstrap = new Bootstrap($request);


$bootstrap->drupalBootstrap(Bootstrap::DRUPAL_BOOTSTRAP_FULL);
menu_execute_active_handler();
