<?php

namespace Drupal\Core\HttpFoundation;

use Symfony\Component\HttpFoundation\Request;


class DrupalRequest extends Request {

  // Drupal has always allowed settings.php to explicitly name the $base_url
  // Symfony would normally build one or return nothing, but Drupal has all
  // sorts of calls to global $base_url - so lets allow it to be set here
  // and have a global request object
  public function setBaseUrl($base_url) {
    $this->baseUrl = $base_url;
  }

  /**
  * Validates that the request host name is safe.
  *
  * @return
  *  TRUE if only containing valid characters, or FALSE otherwise.
  */
  public function isValidHttpHost(){
    // As HTTP_HOST is user input, ensure it only contains characters allowed
    // in hostnames. See RFC 952 (and RFC 2181).
    // $_SERVER['HTTP_HOST'] is lowercased here per specifications.
    return preg_match('/^\[?(?:[a-zA-Z0-9-:\]_]+\.?)+$/', strtolower($this->getHttpHost()));
  }

}