<?php

/**
 * @file
 *
 * Drupal Bootstrap
 */

namespace Drupal\Core\Bootstrap;

class Bootstrap {


  /**
   * First bootstrap phase: initialize configuration.
   */
  const DRUPAL_BOOTSTRAP_CONFIGURATION = 0;

  /**
   * Second bootstrap phase: try to serve a cached page.
   */
  const DRUPAL_BOOTSTRAP_PAGE_CACHE = 1;

  /**
   * Third bootstrap phase: initialize database layer.
   */
  const DRUPAL_BOOTSTRAP_DATABASE = 2;

  /**
   * Fourth bootstrap phase: initialize the variable system.
   */
  const DRUPAL_BOOTSTRAP_VARIABLES = 3;

  /**
   * Fifth bootstrap phase: initialize session handling.
   */
  const DRUPAL_BOOTSTRAP_SESSION = 4;

  /**
   * Sixth bootstrap phase: set up the page header.
   */
  const DRUPAL_BOOTSTRAP_PAGE_HEADER = 5;

  /**
   * Seventh bootstrap phase: find out language of the page.
   */
  const DRUPAL_BOOTSTRAP_LANGUAGE = 6;

  /**
   * Final bootstrap phase: Drupal is fully loaded; validate and fix input data.
   */
  const DRUPAL_BOOTSTRAP_FULL = 7;


  function __construct($request) {
    /**
     * Time of the current request in seconds elapsed since the Unix Epoch.
     *
     * This differs from $_SERVER['REQUEST_TIME'], which is stored as a float
     * since PHP 5.4.0. Float timestamps confuse most PHP functions
     * (including date_create()).
     *
     * @see http://php.net/manual/reserved.variables.server.php
     * @see http://php.net/manual/function.time.php
     */
    define('REQUEST_TIME', (int) $request->server->get('REQUEST_TIME'));
    // Set the Drupal custom error handler.
    set_error_handler('_drupal_error_handler');
    set_exception_handler('_drupal_exception_handler');

    // Start a page timer:
    timer_start('page');

    //Initialize the PHP environment
    $this->phpEnvironmentInitialize($request);
    // Initialize the configuration, including variables from settings.php.
    $this->drupalSettingsInitialize($request);
  }


  /**
   * Initializes the PHP environment.
   */
  function phpEnvironmentInitialize($request) {
    if (!$request->server->has('HTTP_REFERER')) {
      $request->server->set('HTTP_REFERER', '');
    }
    if (!$request->server->has('SERVER_PROTOCOL') || ($request->server->get('SERVER_PROTOCOL') != 'HTTP/1.0' && $request->server->get('SERVER_PROTOCOL') != 'HTTP/1.1')) {
      $request->server->set('SERVER_PROTOCOL', 'HTTP/1.0');
    }
    if ($request->server->has('HTTP_HOST')) {
      // As HTTP_HOST is user input, ensure it only contains characters allowed
      // in hostnames. See RFC 952 (and RFC 2181).
      // $_SERVER['HTTP_HOST'] is lowercased here per specifications.
      $request->server->set('HTTP_HOST', strtolower($request->server->get('HTTP_HOST')));
      //@TODO drupal_valid_http_host function should probably be part of Core\Drupal\Request
      if (!drupal_valid_http_host($request->server->get('HTTP_HOST'))) {

        // HTTP_HOST is invalid, e.g. if containing slashes it may be an attack.
        header($request->server->get('SERVER_PROTOCOL') . ' 400 Bad Request');
        exit;
      }
    }
    else {
      // Some pre-HTTP/1.1 clients will not send a Host header. Ensure the key is
      // defined for E_ALL compliance.
      $request->server->set('HTTP_HOST', '');
    }

    // When clean URLs are enabled, emulate ?q=foo/bar using REQUEST_URI. It is
    // not possible to append the query string using mod_rewrite without the B
    // flag (this was added in Apache 2.2.8), because mod_rewrite unescapes the
    // path before passing it on to PHP. This is a problem when the path contains
    // e.g. "&" or "%" that have special meanings in URLs and must be encoded.
    $_GET['q'] = request_path();

    // Enforce E_STRICT, but allow users to set levels not part of E_STRICT.
    error_reporting(E_STRICT | E_ALL | error_reporting());

    // Override PHP settings required for Drupal to work properly.
    // sites/default/default.settings.php contains more runtime settings.
    // The .htaccess file contains settings that cannot be changed at runtime.

    // Don't escape quotes when reading files from the database, disk, etc.
    ini_set('magic_quotes_runtime', '0');
    // Use session cookies, not transparent sessions that puts the session id in
    // the query string.
    ini_set('session.use_cookies', '1');
    ini_set('session.use_only_cookies', '1');
    ini_set('session.use_trans_sid', '0');
    // Don't send HTTP headers using PHP's session handler.
    ini_set('session.cache_limiter', 'none');
    // Use httponly session cookies.
    ini_set('session.cookie_httponly', '1');

    // Set sane locale settings, to ensure consistent string, dates, times and
    // numbers handling.
    setlocale(LC_ALL, 'C');
  }


  /**
   * Sets the base URL, cookie domain, and session name from configuration.
   */
  function drupalSettingsInitialize($request) {
    global $base_url, $base_path, $base_root;

    // Export the following settings.php variables to the global namespace
    global $databases, $cookie_domain, $conf, $installed_profile, $update_free_access, $db_url, $db_prefix, $drupal_hash_salt, $is_https, $base_secure_url, $base_insecure_url;
    $conf = array();

    if (file_exists(DRUPAL_ROOT . '/' . conf_path() . '/settings.php')) {
      include_once DRUPAL_ROOT . '/' . conf_path() . '/settings.php';
    }
    $is_https = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on';

    if (isset($base_url)) {
      // Parse fixed base URL from settings.php.
      $parts = parse_url($base_url);
      $http_protocol = $parts['scheme'];
      if (!isset($parts['path'])) {
        $parts['path'] = '';
      }
      $base_path = $parts['path'] . '/';
      // Build $base_root (everything until first slash after "scheme://").
      $base_root = substr($base_url, 0, strlen($base_url) - strlen($parts['path']));
    }
    else {
      // Create base URL
      $http_protocol = $is_https ? 'https' : 'http';
      $base_root = $http_protocol . '://' . $_SERVER['HTTP_HOST'];

      $base_url = $base_root;

      // $_SERVER['SCRIPT_NAME'] can, in contrast to $_SERVER['PHP_SELF'], not
      // be modified by a visitor.
      if ($dir = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/')) {
        // Remove "core" directory if present, allowing install.php, update.php,
        // cron.php and others to auto-detect a base path.
        $core_position = strrpos($dir, '/core');
        if ($core_position !== FALSE && strlen($dir) - 5 == $core_position) {
          $base_path = substr($dir, 0, $core_position);
        }
        else {
          $base_path = $dir;
        }
        $base_url .= $base_path;
        $base_path .= '/';
      }
      else {
        $base_path = '/';
      }
    }
    $base_secure_url = str_replace('http://', 'https://', $base_url);
    $base_insecure_url = str_replace('https://', 'http://', $base_url);

    if ($cookie_domain) {
      // If the user specifies the cookie domain, also use it for session name.
      $session_name = $cookie_domain;
    }
    else {
      // Otherwise use $base_url as session name, without the protocol
      // to use the same session identifiers across http and https.
      list( , $session_name) = explode('://', $base_url, 2);
      // HTTP_HOST can be modified by a visitor, but we already sanitized it
      // in drupal_settings_initialize().
      if (!empty($_SERVER['HTTP_HOST'])) {
        $cookie_domain = $_SERVER['HTTP_HOST'];
        // Strip leading periods, www., and port numbers from cookie domain.
        $cookie_domain = ltrim($cookie_domain, '.');
        if (strpos($cookie_domain, 'www.') === 0) {
          $cookie_domain = substr($cookie_domain, 4);
        }
        $cookie_domain = explode(':', $cookie_domain);
        $cookie_domain = '.' . $cookie_domain[0];
      }
    }
    // Per RFC 2109, cookie domains must contain at least one dot other than the
    // first. For hosts such as 'localhost' or IP Addresses we don't set a cookie domain.
    if (count(explode('.', $cookie_domain)) > 2 && !is_numeric(str_replace('.', '', $cookie_domain))) {
      ini_set('session.cookie_domain', $cookie_domain);
    }
    // To prevent session cookies from being hijacked, a user can configure the
    // SSL version of their website to only transfer session cookies via SSL by
    // using PHP's session.cookie_secure setting. The browser will then use two
    // separate session cookies for the HTTPS and HTTP versions of the site. So we
    // must use different session identifiers for HTTPS and HTTP to prevent a
    // cookie collision.
    if ($is_https) {
      ini_set('session.cookie_secure', TRUE);
    }
    $prefix = ini_get('session.cookie_secure') ? 'SSESS' : 'SESS';
    session_name($prefix . substr(hash('sha256', $session_name), 0, 32));
  }

  var $phases = array(
      Bootstrap::DRUPAL_BOOTSTRAP_PAGE_CACHE,
      Bootstrap::DRUPAL_BOOTSTRAP_DATABASE,
      Bootstrap::DRUPAL_BOOTSTRAP_VARIABLES,
      Bootstrap::DRUPAL_BOOTSTRAP_SESSION,
      Bootstrap::DRUPAL_BOOTSTRAP_PAGE_HEADER,
      Bootstrap::DRUPAL_BOOTSTRAP_LANGUAGE,
      Bootstrap::DRUPAL_BOOTSTRAP_FULL,
    );

  var $finalPhase;
  // Not drupal_static(), because it's impossible to roll back to an earlier
  // bootstrap state.
  var $storedPhase = -1;

  public function __get($name) {
    return $this->$name;
  }
   /**
   * Ensures Drupal is bootstrapped to the specified phase.
   *
   * The bootstrap phase is an integer constant identifying a phase of Drupal
   * to load. Each phase adds to the previous one, so invoking a later phase
   * automatically runs the earlier phases as well. To access the Drupal
   * database from a script without loading anything else, include bootstrap.inc
   * and call drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE).
   *
   * @param $phase
   *   A constant. Allowed values are the DRUPAL_BOOTSTRAP_* constants.
   * @param $new_phase
   *   A boolean, set to FALSE if calling drupal_bootstrap from inside a
   *   function called from drupal_bootstrap (recursion).
   *
   * @return
   *   The most recently completed phase.
   */
  public function drupalBootstrap($phase = NULL, $new_phase = TRUE) {


    // When not recursing, store the phase name so it's not forgotten while
    // recursing.
    if ($new_phase) {
      $this->finalPhase = $phase;
    }
    if (isset($phase)) {
      // Call a phase if it has not been called before and is below the requested
      // phase.
      while ($this->phases && $phase > $this->storedPhase && $this->finalPhase > $this->storedPhase) {
        $current_phase = array_shift($this->phases);

        // This function is re-entrant. Only update the completed phase when the
        // current call actually resulted in a progress in the bootstrap process.
        if ($current_phase > $this->storedPhase) {
          $this->storedPhase = $current_phase;
        }

        switch ($current_phase) {

          case Bootstrap::DRUPAL_BOOTSTRAP_PAGE_CACHE:
            $this->bootstrapPageCache();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_DATABASE:
            $this->bootstrapDatabase();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_VARIABLES:
            $this->bootstrapVariables();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_SESSION:
            require_once DRUPAL_ROOT . '/' . variable_get('session_inc', 'core/includes/session.inc');
            drupal_session_initialize();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_PAGE_HEADER:
            _drupal_bootstrap_page_header();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_LANGUAGE:
            drupal_language_initialize();
            break;

          case Bootstrap::DRUPAL_BOOTSTRAP_FULL:
            require_once DRUPAL_ROOT . '/core/includes/common.inc';
            _drupal_bootstrap_full();
            break;
        }
      }
    }
    return $this->storedPhase;
  }

  /**
   * Attempts to serve a page from the cache.
   */
  private function bootstrapPageCache() {
    global $user;

    // Allow specifying special cache handlers in settings.php, like
    // using memcached or files for storing cache information.
    require_once DRUPAL_ROOT . '/core/includes/cache.inc';
    foreach (variable_get('cache_backends', array()) as $include) {
      require_once DRUPAL_ROOT . '/' . $include;
    }
    // Check for a cache mode force from settings.php.
    if (variable_get('page_cache_without_database')) {
      $cache_enabled = TRUE;
    }
    else {
      $this->drupalBootstrap(Bootstrap::DRUPAL_BOOTSTRAP_VARIABLES, FALSE);
      $cache_enabled = variable_get('cache');
    }
    drupal_block_denied(ip_address());
    // If there is no session cookie and cache is enabled (or forced), try
    // to serve a cached page.
    if (!isset($_COOKIE[session_name()]) && $cache_enabled) {
      // Make sure there is a user object because its timestamp will be
      // checked, hook_boot might check for anonymous user etc.
      $user = drupal_anonymous_user();
      // Get the page from the cache.
      $cache = drupal_page_get_cache();
      // If there is a cached page, display it.
      if (is_object($cache)) {
        header('X-Drupal-Cache: HIT');
        // Restore the metadata cached with the page.
        $_GET['q'] = $cache->data['path'];
        drupal_set_title($cache->data['title'], PASS_THROUGH);
        date_default_timezone_set(drupal_get_user_timezone());
        // If the skipping of the bootstrap hooks is not enforced, call
        // hook_boot.
        if (variable_get('page_cache_invoke_hooks', TRUE)) {
          bootstrap_invoke_all('boot');
        }
        drupal_serve_page_from_cache($cache);
        // If the skipping of the bootstrap hooks is not enforced, call
        // hook_exit.
        if (variable_get('page_cache_invoke_hooks', TRUE)) {
          bootstrap_invoke_all('exit');
        }
        // We are done.
        exit;
      }
      else {
        header('X-Drupal-Cache: MISS');
      }
    }
  }

  /**
   * Initializes the database system and registers autoload functions.
   */
  function bootstrapDatabase() {
    // Redirect the user to the installation script if Drupal has not been
    // installed yet (i.e., if no $databases array has been defined in the
    // settings.php file) and we are not already installing.
    if (empty($GLOBALS['databases']) && !drupal_installation_attempted()) {
      include_once DRUPAL_ROOT . '/core/includes/install.inc';
      install_goto('core/install.php');
    }

    // The user agent header is used to pass a database prefix in the request when
    // running tests. However, for security reasons, it is imperative that we
    // validate we ourselves made the request.
    if ($test_prefix = drupal_valid_test_ua()) {
      // Set the test run id for use in other parts of Drupal.
      $test_info = &$GLOBALS['drupal_test_info'];
      $test_info['test_run_id'] = $test_prefix;
      $test_info['in_child_site'] = TRUE;

      foreach ($GLOBALS['databases']['default'] as &$value) {
        // Extract the current default database prefix.
        if (!isset($value['prefix'])) {
          $current_prefix = '';
        }
        elseif (is_array($value['prefix'])) {
          $current_prefix = $value['prefix']['default'];
        }
        else {
          $current_prefix = $value['prefix'];
        }

        // Remove the current database prefix and replace it by our own.
        $value['prefix'] = array(
          'default' => $current_prefix . $test_prefix,
        );
      }
    }

    // Initialize the database system. Note that the connection
    // won't be initialized until it is actually requested.
    require_once DRUPAL_ROOT . '/core/includes/database.inc';

    // Register autoload functions so that we can access classes and interfaces.
    // The database autoload routine comes first so that we can load the database
    // system without hitting the database. That is especially important during
    // the install or upgrade process.
    spl_autoload_register('drupal_autoload_class');
    spl_autoload_register('drupal_autoload_interface');
  }

  /**
   * Loads system variables and all enabled bootstrap modules.
   */
  function bootstrapVariables() {
    global $conf;

    // Initialize the lock system.
    require_once DRUPAL_ROOT . '/' . variable_get('lock_inc', 'core/includes/lock.inc');
    lock_initialize();

    // Load variables from the database, but do not overwrite variables set in settings.php.
    $conf = variable_initialize(isset($conf) ? $conf : array());
    // Load bootstrap modules.
    require_once DRUPAL_ROOT . '/core/includes/module.inc';
    module_load_all(TRUE);
  }
}
